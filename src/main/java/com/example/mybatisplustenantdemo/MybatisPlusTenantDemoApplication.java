package com.example.mybatisplustenantdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisPlusTenantDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusTenantDemoApplication.class, args);
    }

}
